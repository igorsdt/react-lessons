// Generics
const myArray: MyArray<number> = [1,2,3];

// T - type
interface MyArray<T> {
  [N: number]: T

  map<U>(fn: (el: T, index: number, arr: MyArray<T>) => U): MyArray<U>;
}

let variable = myArray[1];

myArray.map((f) => f + 1);
myArray.map((f) => `f + ${f}`);

function identity<T>(arg: T): T {
  return arg;
}

let res = identity(12);

function getLen<T extends Array<any>>(arr: T): number {
  return arr.length;
}

getLen([1, 2, 4]);

interface IValueWithType<T> {
  type: string,
  value: T
}

function withType<U>(arg: U): IValueWithType<U> {
  return {
    type: typeof arg,
    value: arg
  };
}

const result2 = withType(123);

// встроенные generics
interface IExample<T> {
  type: string;
  value: T;
  isEmpty: boolean;
}

// для перечисления свойств, которые нельзя использовать
const omittedObject: Omit<IExample<string>, 'isEmpty' | 'value'> = {
  type: 'sdsd'
};

// вытаскивает только те свойства, которые обязательны
const picked: Pick<IExample<number>, 'isEmpty' | 'value'> = {
  isEmpty: true,
  value: 123
};

// все свойства интерфейса необязательны
const partial: Partial<IExample<object>> = {

};