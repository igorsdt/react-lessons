// JS Types        // typeof

const str = 'string';   // typeof str -> 'string'
const num = 2;          // typeof num -> 'number'
const nan = NaN;        // typeof nan -> 'number'
const obj = {};         // typeof obj -> 'object'
const arr = [];         // typeof arr -> 'object'
const nul = null;       // typeof nul -> 'object'
const sym = Symbol();   // typeof sym -> 'symbol'
const und = undefined;  // typeof und -> 'undefined'
const _void = void 0;   // typeof _void -> 'undefined'
const bool = true;      // typeof bool -> 'boolean'
const fn = () => {};    // typeof fn -> 'function'

let tsStr = 'asd';

// старый вид с проверками
// function sumJS(arr) {
//   if (arr.instanceOf Array) {
//     return arr.reduce((a, y) => a + y);
//   }
//   throw new Error('type mismatch');
// }

// новый вид с TS
function sumTS(arr: number[]) {
  return arr.reduce((a, y) => a + y);
}

// проблемы в нестрогом JS
// 'some' + 2 // -> 'some2'
// 'some' - 2 // -> NaN
// '2' + 2 -> '22'
// '2' - 2 -> NaN

const tsNumber = 2;
const tsString = 'str';

const result = tsString + tsNumber;
// надо заниматься приведением типов при операциях с TS, чтобы избежать проблем
const resultTwo = parseInt(tsString) - tsNumber;

if (typeof tsString === 'number') {
  const resultTwo = tsString - tsNumber;
}

// объявление с несколькими типами данных (Union type)
const strOrNumber: string | number = 2;

// создание type alias
type StrOrNumber = string | number;

const strOrNumber1: StrOrNumber = 1;
const strOrNumber2: StrOrNumber = 2;
const strOrNumber3: StrOrNumber = 3;
const strOrNumber4: StrOrNumber = 4;

type AllJsSimpleTypes = string | number | [] | object | undefined | null | boolean | void | symbol;

// Array
// определение числового массива
const tsArray: number[] = [1,2,3];
// Generic массива
const tsArrayGeneric: Array<number> = [];

// Union Type вариант объявления массива
const unionArray: (string | number)[] = [1, 2, '3'];
// тоже самое только с дженериком
const unionArray2: Array<string | number> = [1, 2, '3'];

// Tuple (массив фиксированной длины)
const myTuple: [number, string] = [1, '2'];

// Object
type MyObjType = { a: number, b: string }; // пример типизации с уже имеющимися знаниями
const myObj: { a: number, b: string } = { a: 1, b: '2'}; // пример типизации с уже имеющимися знаниями
const myObj3 = { a: 1, b: '2'}; // пример типизации с уже имеющимися знаниями

// пример с использованием интерфейса
interface MyFirstInterface {
  readonly a: number, // пометка "только для чтения"
  b: string,
  c?: number[]; // "?" - значит, что значение может отсутствовать (optional type)
}

const myObj2: MyFirstInterface = {
  a: 2,
  b: '123',
  //c: [1]
}

const value = myObj.b;

// метод записи для объекта с неизвестным содержимым (например пришедший из API)
const ApiAnswer: IndexInterface = { key: 'asd', key1: 'asd' };
  // индекс сигнатура
  interface IndexInterface {
    [N: string]: string // "N" - произвольное значение, обозначающее ключ
  }

  const val3 = ApiAnswer.randomKey;

// functions

enum Methods {
  add,
  sub,
}

// calculate('add', 5, 5) // -> 10
function calculate (method: Methods, left: number, right: number): number {
  switch(method) {
    case Methods.add: return left + right;
    case Methods.sub: return left - right; // subtract
  }
}

const sum = calculate(Methods.add, 2, 2);
const sub = calculate(Methods.sub, 2, 2);
// console.log(Meth0ds.add) -> 0

const ArrowFn: TypeFn = () => 2;
type TypeFn = () => number;

// интерфейс для аргументов функции
interface FnInterface {
  (a: number): void;
}
const ArrowFn1: FnInterface = (value) => 2;



// странные типы в TS
type StrangeTsTypes = any | unknown | never;

// any
const some: any = '2'; // любой тип
some.reduce(); // ошибок нет

// unknown
const un: unknown = '2'; // неизвестный тип, не подходит не под один тип
un.reduce(); // ошибка есть, у этого типа нет методов

// never - тип возвращаемого значения у функции (значения нет)
function neverFn(): never {
  throw new Error('some exception');
}

const someValue = neverFn();