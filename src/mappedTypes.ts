const mistake = [] as Array<number>; // все элементы массива должны быть числовые

let bigObject = {
  "commit": {
    "id": "ed899a2f4b50b4370feeea94676502b42383c746",
    "short_id": "ed899a2f4b5",
    "title": "Replace sanitize with escape once",
    "author_name": "Example User",
    "author_email": "user@example.com",
    "authored_date": "2021-09-20T11:50:22.001+00:00",
    "committer_name": "Administrator",
    "committer_email": "admin@example.com",
    "committed_date": "2021-09-20T11:50:22.001+00:00",
    "created_at": "2021-09-20T11:50:22.001+00:00",
    "message": "Replace sanitize with escape once",
    "parent_ids": [
      "6104942438c14ec7bd21c6cd5bd995272b3faff6"
    ],
    "web_url": "https://gitlab.example.com/janedoe/gitlab-foss/-/commit/ed899a2f4b50b4370feeea94676502b42383c746"
  },
  "commits": {
    "id": "6104942438c14ec7bd21c6cd5bd995272b3faff6",
    "short_id": "6104942438c",
    "title": "Sanitize for network graph",
    "author_name": "randx",
    "author_email": "user@example.com",
    "committer_name": "ExampleName",
    "committer_email": "user@example.com",
    "created_at": "2021-09-20T09:06:12.201+00:00",
    "message": "Sanitize for network graph",
    "parent_ids": [
      "ae1d9fb46aa2b07ee9836d49862ec4e2c46fbbba"
    ],
    "web_url": "https://gitlab.example.com/janedoe/gitlab-foss/-/commit/ed899a2f4b50b4370feeea94676502b42383c746"
  },
  "diffs": [{
    "old_path": "files/js/application.js",
    "new_path": "files/js/application.js",
    "a_mode": null,
    "b_mode": "12122",
    "new_file": false,
    "renamed_file": false,
    "deleted_file": false
  }],
  "compare_timeout": false,
  "compare_same_ref": false
};

bigObject.compare_same_ref = true;

type TMyBigObject = typeof bigObject;

const typedBigObject: MyReadonly<TMyBigObject> = bigObject;

typedBigObject.compare_same_ref = false;
typedBigObject.commit.id = '123';

type TObjKeys = keyof TMyBigObject;
type TCommitType = TMyBigObject['commit'];

type MyReadonly<T> = {
  // mapped types, для перебора типа
  readonly [N in keyof T]: T[N];
};

// for (let N of ['asd', 'qwe']) {}

// const some: MyReadonly<TMyBigObject> = {
//   compare_same_ref: true
// };

// все ключи в объекте не обязательны (свой partial)
type MyPartial<T> = {
  [N in keyof T]?: T[N];
};

// свой pick
type MyPick<T, K extends keyof T> = {
  [N in K]: T[N];
};

// type picked = Pick<TMyBigObject, 'commit'>;
type picked = MyPick<TMyBigObject, 'commit' | 'commits'>;

// свой readonly для объекта и его ключей, не зависит от вложености
type MyReadonlyDeep<T> = {
  // mapped types, для перебора типа
  readonly [N in keyof T]: T[N] extends object ? MyReadonlyDeep<T[N]> : T[N];
};

const typedBigObjectDeep: MyReadonlyDeep<TMyBigObject> = bigObject;

typedBigObjectDeep.compare_same_ref = false;
typedBigObjectDeep.commit.id = '123';

type TSomeType = MyReadonlyDeep<TMyBigObject>;

// type inference
type RemoveReadonly<T> = T extends MyReadonlyDeep<infer E> ? E : T;

type TTest = RemoveReadonly<TSomeType>;

function greaterThenZero(a: number, b: string): boolean {
  return a > 0;
}

type FnReturnType<T> = T extends ((... args: any[]) => infer R) ? R : never;
type FnReturnParameters<T> = T extends ((... args: infer R) => any) ? R : never;

type TReturnType = FnReturnType<typeof greaterThenZero>;
type TArgsType = FnReturnParameters<typeof greaterThenZero>;